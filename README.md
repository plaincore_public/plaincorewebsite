# Plaincore Robotics & Embedded Systems Website

[![pipeline status](https://gitlab.com/plaincore_public/plaincorewebsite/badges/main/pipeline.svg)](https://gitlab.com/plaincore_public/plaincorewebsite/-/commits/main) 
 
This website uses hugo for static content generation. https://gohugo.io/

The used Hugo theme is an adapted Version of the hugo-serif-theme: https://github.com/zerostaticthemes/hugo-serif-theme

Have a nice day!
