---
title: Impressum
date: 2018-02-22T17:01:34+07:00
---
# Angaben gemäß §5 TMG:

TTI – Technologie-Transfer-Initiative GmbH an der Universität Stuttgart (TTI GmbH)

Transfer- und Gründerunternehmung PlainCore Robotics & Embedded Systems
Nobelstraße 15
70569 Stuttgart
Germany

Inhaltlich Verantwortlicher im Sinne des Presserechts nach § 55 RStV und vertreten durch: Mark Geiger

Registereintrag & Umsatzsteuer-ID:

Amtsgericht Stuttgart HRB 19455

USt-IdNr. DE 194532993

Geschäftsführer: Dipl.-Ing., MBA Peter Heinke

Kontakt: Mark Geiger, Tel.: +49 (0)1578 9077035, E-Mail: info@plaincore.de or mark.geiger@plaincore.de

www.plaincore.de

# Haftungsausschluss

Die Inhalte dieser Website werden mit größtmöglicher Sorgfalt recherchiert und implementiert. Fehler im Bearbeitungsvorgang sind dennoch nicht auszuschließen. Hinweise und Korrekturen senden Sie bitte an info@plaincore.de

Eine Haftung für die Richtigkeit, Vollständigkeit und Aktualität dieser Webseiten kann trotz sorgfältiger Prüfung nicht übernommen werden. Die TTI GmbH Transfer- und Gründerunternehmung PlainCore  (im Folgenden TGU genannt) übernimmt insbesondere keinerlei Haftung für eventuelle Schäden oder Konsequenzen, die durch die direkte oder indirekte Nutzung der angebotenen Inhalte entstehen.

# Linkdistanzierung

Die TTI GmbH TGU PlainCore Robotics & Embedded Sytems (kurz TTI GmbH TGU PlainCore) ist als Inhaltsanbieter für die eigenen Inhalte, die sie zur Nutzung bereithält, nach den allgemeinen Gesetzen verantwortlich. Von diesen eigenen Inhalten sind Querverweise (“externe Links”) auf die von anderen Anbietern bereitgehaltenen Inhalte zu unterscheiden. Diese fremden Inhalte stammen weder von der TTI GmbH TGU PlainCore  noch hat sie die Möglichkeit, den Inhalt von Seiten Dritter zu beeinflussen. Die Inhalte fremder Seiten, auf die die TTI GmbH TGU PlainCore  mittels Links hinweist, spiegeln nicht die Meinung der TTI GmbH TGU PlainCore  wider, sondern dienen lediglich der Information und der Darstellung von Zusammenhängen.

Die TTI GmbH TGU PlainCore  distanziert sich hiermit ausdrücklich von allen Inhalten aller gelinkten fremden Seiten auf ihrer Website und macht sich deren Inhalte nicht zu Eigen. Diese Feststellungen gelten für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie für Fremdeinträge in die von der TTI GmbH TGU PlainCore  eingerichteten Mailinglisten. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde.

# Hinweis gemäß § 36 Verbraucherstreitbeilegungsgesetz (VSBG)

Die TTI GmbH TGU PlainCore  wird nicht an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle im Sinne des deutschen Verbraucherstreitbeilegungsgesetzes (VSBG) teilnehmen und ist hierzu auch nicht verpflichtet.

# Technische Realisierung und Providing

Content Management: Hugo CMS

Hosting: Netcup GmbH

Erstellung: Mark Geiger

Copyright Inhalt

TTI – Technologie-Transfer-Initiative GmbH an der Universität Stuttgart
Transfer- und Gründerunternehmung PlainCore Robotics & Embedded Systems

Nobelstraße 15
70569 Stuttgart
Telefon: +49 (0)578 9077035
E-Mail: info@plaincore.de

# Copyright Bilder

Wir verwenden lizenzfreie Bilder der Plattformen Pixabay und Pexels