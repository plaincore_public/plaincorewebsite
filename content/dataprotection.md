---
title: Datenschutz
date: 2018-02-22T17:01:34+07:00
---
Privacy statement for the Website www.plaincore.de

# Preamble

The protection of your data is important to us and we know that it is also important to you. Read our privacy policy to obtain information about how TTI GmbH collects, uses, stores and protects your personal information and what rights you have as the person affected. The operators of these pages take the protection of your personal data very seriously. We treat your personal data confidentially and in accordance with the statutory data protection regulations and this privacy policy. This privacy policy applies to the use of the website https://plaincore.de

# Name and contact details of the controller and the company data protection officer

The person responsible within the meaning of the General Data Protection Regulation and other national data protection laws of the member states as well as other data protection regulations is the:

TTI – Technology Transfer Initiative GmbH TGU PlainCore Robotics & Embedded Systems at the University of Stuttgart (hereinafter: TTI TGU PlainCore), Nobelstraße 15, 70569 Stuttgart, Germany

E-mail: [datenschutz@tti-stuttgart.de](mailto:datenschutz@tti-stuttgart.de)

Website: www.plaincore.de

The company data protection officer is under the o.g. Address and e-mail available.

# General information about data processing

In principle, we collect and use personal data of our users only to the extent necessary to provide a functional website and our content and services. The use of our website is usually possible without providing personal information. As far as on our sides personal data (for example name, address or E-Mail addresses) are raised, this takes place regularly only after consent of the user. An exception applies to cases in which prior consent is not possible for reasons of fact and the processing of the data is permitted by law. These data will not be disclosed to third parties without your explicit consent. Please note that data transmission over the Internet (for example, when communicating by e-mail) may have security vulnerabilities. A complete protection of the data from access by third parties is not possible.

Insofar as we obtain the consent of the data subject for processing of personal data, Art. 6 para. 1 lit. a EU General Data Protection Regulation (DSGVO) as legal basis. In the processing of personal data necessary for the performance of a contract of which the data subject is a party, Art. 6 para. 1 lit. b DSGVO as legal basis. This also applies to processing operations required to carry out pre-contractual measures. Insofar as the processing of personal data is required to fulfill a legal obligation that our company is subject to, Art. 6 para. 1 lit. c DSGVO as legal basis. In the event that vital interests of the data subject or another natural person require the processing of personal data, Art. 6 para. 1 lit. d DSGVO as legal basis. If processing is necessary to safeguard the legitimate interests of our company or a third party, and if the interest, fundamental rights and freedoms of the data subject do not prevail over the first interest, Art. 6 para. 1 lit. f DSGVO as legal basis for processing. The personal data of the data subject will be deleted or blocked as soon as the purpose of the storage is deleted. In addition, it may be stored if provided for by the European or national legislator in EU regulations, laws or other regulations to which the controller is subject. A blocking or deletion of the data takes place even if a storage period prescribed by the mentioned standards expires, unless there is a need for further storage of the data for a contract conclusion or a contract fulfillment. Please note that data transmission over the Internet (for example, when communicating by e-mail) may have security vulnerabilities. A complete protection of the data from access by third parties is not possible.

# Collection and storage of personal data as well as the nature and purpose of their use

When visiting the website When you visit our website www.plaincore.de, the browser used on your device automatically sends information to the server of our website. The following information is collected without your intervention and stored until automated deletion: IP address of the requesting computer, Date and time of access, Name and URL of the retrieved file, Website from which access is made (referrer URL), Websites that are accessed by the user’s system through our website, used browser and, if applicable, the operating system of your computer as well as the name of your access provider. This information is temporarily stored in a so-called log file. In no case we use the collected data for the purpose of drawing conclusions about you. There is no merge of this data with other data sources. We reserve the right to check this data retrospectively, if we become aware of specific indications for illegal use.

The legal basis for the temporary storage of data and log files is Art. 6 para. 1 p. 1 lit. f DSGVO. The data mentioned are processed by us for the following purposes: Ensuring a smooth connection setup and delivery of the website to the user’s computer, Ensuring the functionality of our website, Ensuring the security of our information technology systems as well Optimization of the website Evaluation of system security and stability. Our legitimate interest follows from the data collection purposes listed above. For these purposes, our legitimate interest in the processing of data according to Art. 6 para. 1 lit. f DSGVO. The data will be deleted as soon as it is no longer necessary to achieve the purpose of its collection. In the case of collecting the data for providing the website, this is the case when the respective session is completed. In the case of storing the data in log files, this is the case after no more than seven days. An additional storage is possible. In this case, the IP addresses of the users are deleted or alienated, so that an assignment of the calling client is no longer possible. The collection of the data for the provision of the website and the storage of the data in log files is essential for the operation of the website. There is consequently no contradiction on the part of the user.

# Disclosure of data

A transfer of your personal data to third parties for purposes other than those listed below does not take place. We only share your personal information with third parties if: as part of an express consent granted by you pursuant to Art. 6 para. 1 sentence 1 lit. a GDPR, if the transmission for the performance of offers or services which you want to claim, in accordance with Art. 6 para. 1 sentence 1 lit. f DSGVO is indispensable and there is no reason to assume that you have a predominantly legitimate interest in non-disclosure, in the event that disclosure pursuant to Art. 6 para. 1 sentence 1 lit. c DSGVO a legal obligation exists, as well if permitted by law and according to Art. 6 para. 1 sentence 1 lit. b DSGVO is required for the settlement of contractual relationships with you.

# cookies

We do not use cookies on our site. 

# Social Media Plug-ins

We do not integrate any third party social media plugins.

The data protection supervisory authority responsible for us can be reached under:

The State Commissioner for Data Protection and Freedom of Information Baden-Württemberg, PO Box 10 29 32, 70025 Stuttgart, Königstrasse 10a, 70173 Stuttgart, Tel .: 0711/61 55 41 – 0, Fax: 0711 / 61 55 41 – 15, E-Mail: poststelle@lfdi.bwl.de, Internet: https: //www.baden-wuerttemberg.datenschutz.de

If you would like to exercise your rights of information and rectification, please contact us by e-mail at: info@plaincore.de or by mail: TTI GmbH TGU PlainCore, Nobelstraße 15, 70569 Stuttgart.

# Right of objection

If your personal data are based on legitimate interests in accordance with Art. 6 para. 1 sentence 1 lit. f DSGVO are processed, you have the right to file an objection against the processing of your personal data in accordance with Art. 21 DSGVO, provided that there are reasons for this arising from your particular situation or the objection is directed against direct mail. In the latter case, you have a general right of objection, which is implemented by us without stating a particular situation. If you would like to exercise your right of revocation or objection, please send an e-mail to info@plaincore.de or contact us by mail: TTI GmbH TGU PlainCore, Nobelstraße 15, 70569 Stuttgart.

# Data Security

We use the widely used Secure Socket Layer (SSL) method in conjunction with the highest level of encryption supported by your browser. In general, this is a 256-bit encryption. If your browser does not support 256-bit encryption, we’ll use 128-bit v3 technology instead. Whether a single page of our website is transmitted in encrypted form is indicated by the closed representation of the key or lock symbol in the lower status bar of your browser. If SSL encryption is enabled, the data you submit to us can not be read by third parties. We also take appropriate technical and organizational security measures to protect your data against accidental or intentional manipulation, partial or total loss, destruction or against unauthorized access by third parties. Our security measures are continuously improved in line with technological developments.

# Updating and changing this privacy policy

This privacy policy is currently valid and is valid as of June 2022. Due to the further development of our website and offers or due to changed legal or regulatory requirements, it may be necessary to change this privacy policy. The latest privacy policy can be viewed and printed at any time on the website at https://plaincore.de/dataprotection

# Contradiction unsolicited advertising / information materials

The use of contact data published in the context of the imprint obligation for the purpose of sending unsolicited advertising and information materials is hereby rejected. The operators of the pages expressly reserve the right to take legal action in the event of the unsolicited sending of advertising information, such as spam e-mails.
