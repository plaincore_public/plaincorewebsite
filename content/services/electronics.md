---
title: Electronics
draft: false
featured: true
date: 2018-11-18T12:33:46+10:00
weight: 6
advanced: Test
image: /images/services/electronics.svg
advanced_image: /images/services/robotics.svg
---
* **Consulting**
* **Design & Layout**
* **Board manufacturing**

&NewLine;

For our own product development we plan, design and layout our electronics completely by ourselves. External experts take over a review and examination of the self-provided designs and layouts for us still to the conclusion. We are also happy to take over your electronics development in our process and develop your electronics according to your specifications.llungen.
