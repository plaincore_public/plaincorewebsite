---
title: IoT-Solutions
draft: false
featured: true
date: 2018-11-28T15:14:54+10:00
weight: 4
advanced: nichts
image: /images/services/iot.svg
advanced_image: /images/services/robotics.svg
---
* **Modems**
* **NB-IoT / LTE / LoRa**
* **Low Power**
* **Security**

&NewLine;

IoT products are our core business. You can use our PlainSense Gateway or develop your own product. In both cases we support you with our know-how. Our experience in low power hardware and software enables us to operate battery powered IoT devices for many years without maintenance or battery replacement. Depending on the application, up to 20 years of autonomous battery operation are possible.
