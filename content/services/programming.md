---
title: Software
draft: false
featured: true
date: 2018-11-28T15:15:34+10:00
weight: 2
advanced: Teste2
image: /images/services/programming.svg
advanced_image: /images/uploads/code.png
---
* **Embedded Software Development**
* **C/C++, Java, Python, etc.**
* **Real-Time OS**
* **Testing**

&NewLine;

The software development takes an ever larger value in many product developments. For many products the software is the most complex and critical part of the development. Our focus is especially on embedded systems. We are happy to develop software in close cooperation with your team or on our own. High software quality is our top priority.

