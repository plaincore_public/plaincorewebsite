---
title: "Robotics"
date: 2018-11-28T15:14:54+10:00
featured: true
draft: false
image: /images/services/robotics.svg
weight: 3
advanced: "Test"
advanced_image: /images/services/robotics.svg
---
- **Sensor technology**
- **Control**
- **Algorithms**
- **Artificial Intelligence**

&NewLine;

Robots are usually highly complex systems that consist of many different components. Typical robotics tasks include sensing, actuation, motion planning, image processing, and more. During this process, strict requirements for real-time capability, robustness and fault tolerance often apply. With our extensive know-how, we are happy to support you in your projects.
