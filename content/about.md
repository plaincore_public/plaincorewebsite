---
title: About us
date: 2022-06-11T07:06:16.232Z
---
# PlainCore Robotics & Embedded Systems

Our main product is the PlainSense Gateway, an IoT gateway to automatically transfer data from external sources, e.g. own sensors to your own server in a simple and straightforward way. We are happy to implement customizations to support exactly your sensor type.

<img class="image-text" src="/images/uploads/electronics2.jpg" alt="drawing" style="width:500px;"/>

## Services

In addition to our own product development, we also help other companies with their development projects. We are happy to accompany you right from the project planning stage and support you throughout the entire process, right up to production. Transparency and quality are especially important to us. Therefore, there are no secrets with us. With our products we deliver the complete source code, schematics and design data. Convince yourself of the quality of our products and services.

## TTI GmbH

We are part of the Technologie-Transfer-Initiative GmbH in the form of a Transfer- und Gründerunternehmung (TGU). TTI GmbH supports start-up projects and is a company of the University of Stuttgart.

![TTI GmbH](/images/uploads/tti.gif "TTI GmbH ")
