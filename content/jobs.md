---
title: Stellenangebote
date: 2022-06-11T07:23:51.082Z
intro_image: images/uploads/electronics3.jpg
intro_image_absolute: true
intro_image_hide_on_mobile: false
---
## Wer sind wir?

We are a young company that develops its own IoT products and helps other companies to bring their products to market as quickly as possible. We support customers in all necessary development steps from project planning to the manufacturing of their products.

Employees gain insight into developing their own products from the ground up and equally gain insight into the realization of customer projects.

<img class="image-text" src="/images/uploads/electronics.jpg" alt="drawing" style="width:500px;"/>

## Who are we looking for?

Currently we are looking for motivated employees from the following areas:

* Software Developer C++ (m/f/d)
* Electronics Developer (m/f/d)
* Compulsory interns or working students (m/f/d)

Contract type: It depends on your participation, just talk to us! Student contracts, mini-jobs, freelance work. Everything is possible with us.

Contact us via: info@plaincore.de
