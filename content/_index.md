---
title: 'Home'
meta_title: 'PlainCore Robotics & Embedded Systems'
description: "Realize your IoT product in the shortest possible time with OpenSource hardware and software!"
intro_image: "images/illustrations/trans.png"
intro_image_absolute: false
intro_image_hide_on_mobile: false
---

