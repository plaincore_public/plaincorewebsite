---
title: Contact
date: 2018-02-22T17:01:34+07:00
layout: contact
---
Feel free to contact us by phone during the indicated opening hours or by email at any time.

| Day        | Opening hours   |
| ---------- | --------------- |
| Monday     | 8:30am - 5:00pm |
| Tuesday    | 8:30am - 5:00pm |
| Wednesday  | 8:30am - 5:00pm |
| Thursday   | 8:30am - 5:00pm |
| Friday     | 8:30am - 5:00pm |
| Saturday   | Closed          |
| Sunday     | Closed          |

<br>

**TTI – Technologie-Transfer-Initiative GmbH an der Universität Stuttgart (TTI GmbH)**

**Transfer- und Gründerunternehmung PlainCore Robotics & Embedded Systems**

Nobelstraße 15

70569 Stuttgart

<br>

Contact: Mark Geiger, Tel.: +49 (0)1578 9077035,

E-Mail: info@plaincore.de or mark.geiger@plaincore.de

https://plaincore.de
