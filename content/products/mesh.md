---
title: "PlainMesh Node"
date: 2018-11-28T15:14:54+10:00
featured: true
draft: false
image: /images/products/coming_soon.jpg

features: |
    - Geehy APM32 microcontroller + neo.cortec NC1000
    - Sub 1GHz frequency
    - Local Mesh Communication
    - Low Power + Low Cost
    - Open schematic, layout + software
    - Freely programmable
weight: 3
---
<br>

The product is currently still in the specification phase. More information will follow soon.
