---
title: "PlainSense Gateway IoT Kit"
date: 2018-11-28T15:14:54+10:00
featured: true
draft: false
image: /images/uploads/plainsense_1.png
features: |
    - STM32 microcontroller + Quectel BG95 cellular modem
    - NB-IoT / LTE / GSM / GNSS
    - Low power battery operation
    - Open schematic, layout + software
    - Freely programmable
    - EEprom for production and calibration data
    - Sensor interface via SPI / I2C / UART / GPIO / ADC
    - IMU
weight: 1
---

The core of the product is a STM32L0xx microcontroller and a Quectel BG95M3 cellular modem.

Typical use cases for a PlainSense Gateway are companies / startups or hobbyists who want to collect sensor data and forward it to an external server as autonomously as possible.

The PlainSesne Gateway is self-programmable, but comes with extensive open standard software. You have full control over the device at all times and can quickly integrate your own sensor type. We are happy to assist you with this.

**Typical use cases are for example:**
- 12-hourly activation of the system, collection of sensor data and transmission to the customer server
- Autonomous data transmission from various fields: Agriculture, Smart City, Industry, ...
- Monitoring / Logging

**Features:**
- NB-IoT / LTE / GSM
- GNSS
- Low power battery operation
- Very fast time to market of your IoT product by ready gateway HW and open software
- EEprom for production and calibration data
- Bootloader for firmware updates
- Sensor interface via SPI / I2C / UART / GPIO / ADC

The PlainSense Gateway is currently under development. Contact us and become a partner in the development, then we can ensure that the product supports your use cases.

The complete electrical design, schematic and layout are [published here](https://gitlab.com/plaincore_public/plainsensegateway).

Contact and price inquiry via: info@plaincore.de
