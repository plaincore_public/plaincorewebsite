---
date: 2022-06-11T07:06:16.232Z
meta_title: Open IoT Initative - PlainCore Robotics & Embedded Systems
description: 'Open software and hardware for the IoT.'
---

<div class="container">
  <img class="intro-image " src="/images/uploads/openiot.png" alt="drawing" style="width:700px;"/>

  <div class="row justify-content-start">

  <div class="linebreak"> </div>
  <p>
    <h2>What's behind the initiative?</h2>
  </p>
  </div>

<div class="row justify-content-start">
<p>
With the OpenIoT initiative, we want to motivate all participants in the IoT industry to share source code, schematics, valuable knowledge and other helpful information related to IoT with everyone. This has incredible potential to save unnecessary resource consumption in the development of more IoT products. We all benefit from this!
<br><br>
At PlainCore, we're already leading by example. Feel free to check out our open source releases, they are [published here](https://gitlab.com/plaincore_public)
</p>

<h2>What is planned</h2>

<p>
We are preparing a central knowledge base where all Open Soruce releases brought to our attention will be compiled. We are happy about any open source contribution to it. We would be happy to link your contribution to it! Please contact us and subscribe to our newsletter!
</p>


</div>
</div>

<div class="linebreak"> </div>

<form action="" method="GET">
 <div class="containerNewsletter">
   <h2>Subscribe to our IoT-Newsletter</h2>
   <p>Monthly news about IoT, Open Source and news from PlainCore</p>
 </div>

 <div class="containerNewsletter">
   <input type="text" placeholder="Name" name="name" required>
   <input type="text" placeholder="Email address" name="mail" required>
 </div>

 <div class="containerNewsletter">
   <input id="bsub" type="button" class="inputBackgroundGood" value="Subscribe" onClick="subscribe(this.form)">
 </div>
</form>

<script type="text/javascript">
const validateEmail = (email) => {
  return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

function subscribe (form) {
    var element = document.getElementById("bsub");
    var newone = element.cloneNode(true);
    element.parentNode.replaceChild(newone, element);

    newone.classList.remove("shake");
    newone.classList.remove("animated");
    newone.classList.remove("inputBackgroundBad");
    newone.classList.remove("inputBackgroundGood");
    if (form.name.value && form.mail.value && validateEmail(form.mail.value))
    {
        newone.classList.add("inputBackgroundGood");
        const Http = new XMLHttpRequest();
        const url='https://api.plaincore.de/newsletter?name=' + form.name.value + '&email=' + form.mail.value;
        Http.open("GET", url);
        Http.send();
        alert ("Registered to Newsletter. Thank you :)");
    } else {
        newone.classList.add("shake");
        newone.classList.add("animated");
        newone.classList.add("inputBackgroundBad");
    }
}
</script>
