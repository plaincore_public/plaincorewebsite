---
title: "Open IoT Initiative takes off!"
date: 2018-11-28T15:14:54+10:00
featured: true
draft: false
image: /images/uploads/openiot.png
short: |
    - Open Source Hardware & Software
    - Schematics, Layouts, Firmware
    - Dare more Open Source
weight: 1
---

Updates & a newsletter about the initiative will be published on an extra page, here you can find [more information about the initiative](https://plaincore.de/openiot/).

Join the Open Source Initiative and contact us.
