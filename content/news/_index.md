---
title: 'News & Blog'
description: 'Embedded engineering, prototype development, engineering office, software development, electronics'
intro_image: images/uploads/news.png
intro_image_absolute: false
intro_image_hide_on_mobile: false
---

# News from PlainCore

News from PlainCore and interesting facts from the IoT world.
